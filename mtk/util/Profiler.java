package mtk.util;

import java.util.*;

/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class Profiler {
    private static Logger log = new Logger(Profiler.class);
    private static HashMap<String, Long> begin = new HashMap<>();
    private static HashMap<String, Info> total = new HashMap<>();
    private static HashMap<Integer, Integer> states = new HashMap<>();
    public static synchronized void add(int state) {
        Integer s = states.get(state);
        if (s == null){
            s = 0;
        }
        states.put(state, s + 1);
    }


    static class Info implements Comparable<Info>{
        private String k;
        private long v;
        private int count;

        public Info(String k, Long v, Integer count) {
            this.k = k;
            this.v = v;
            this.count = count;
        }

        @Override
        public int compareTo(Info i) {
            return v > i.v ? -1: 1;
        }
    }
    public static void begin(String type){
        synchronized(begin) {
            begin.put(type + Thread.currentThread().getId(), System.currentTimeMillis());
        }
    }
    public static void end(String type){
        Long start;
        synchronized(begin) {
            start = begin.remove(type + Thread.currentThread().getId());
            if (start == null) {
                log.error("Begin is null for " + type + Thread.currentThread().getId());
                return;
            }
        }
        long t = System.currentTimeMillis() - start;
        if (t > 0) {
            add(type, t);
        }
    }

    public static void add(String tt, long t) {
        if (tt == null) {
            tt = "unknown";
        }else if (tt.length() > 50){
            tt = tt.substring(0, 50);
        }

        synchronized(total) {
            Info info = Profiler.total.get(tt);
            if (info == null) {
                info = new Info(tt, t, 1);
                total.put(tt, info);
                return;
            }
            info.count++;
            info.v += t;
        }
    }


    public static void printInfo() {
        StringBuilder sb = new StringBuilder();
        Set<Map.Entry<String, Info>> set;
        Set<Map.Entry<Integer, Integer>> st;
        try{
            synchronized(states) {
                st = states.entrySet();
            }
            for(Map.Entry<Integer, Integer> e: st){
                sb.append("Status " + e.getKey() + ": " + e.getValue() + "\n");
            }
        }catch (Exception e){
            log.error(e);

        }
        try{
            synchronized(total) {
                set = total.entrySet();
            }
            ArrayList<Info> list = new ArrayList<>();
            for(Map.Entry<String, Info> e: set){
                if (e.getValue() == null){
                    continue;
                }
                list.add(e.getValue());
            }
            Collections.sort(list);
            for (Info i: list){
                sb.append(i.v / 1000 + " sec. /" + i.count + " times: " + i.k + "\n");
            }

        }catch (Exception e){
            log.error(e);
        }

        sb.append("*********************");
        log.info(sb.toString());
    }
}
